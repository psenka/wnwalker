import sys
from pwstructure import *

if len(sys.argv) != 3:
    raise Exception("Usage : wnwalker.py input_file output_file")

pws = PWStructure(sys.argv[1])

ofile = open(sys.argv[2], 'w+')
ofile.write(pws.__str__())
ofile.close()
