import re


class PragmaWarning:
    def __init__(self, line, type, level, locus):
        self._level = level
        self._locus =  locus
        self._line = line
        if any(type is v for v in PragmaWarning.types):
            self._type = type
        else:
            self._type = "Unknown"


    def __str__(self):
        return "{0:10d} {1:2d}|".format(self._line, self._level) + \
            ("".join(
            [PragmaWarning.cfg_indent for i in range(self._level)]
            ) + self._type).ljust(PragmaWarning.cfg_ljust) + \
            self._locus
    

    def __eq__(self, other):
        return self._type == other._type

    cfg_indent = "  "
    cfg_ljust = 30 
    types = ("Push", "Pop", "Disable", "Once", "Default", "Suppress", "Error")


class PragmaWarningSpecific(PragmaWarning):


    def __init__(self, line, type, level, locus, wnnr):
        if any(type is v for v in PragmaWarningSpecific.specific_types):
            PragmaWarning.__init__(self, line, type, level, locus)
            self._wnnr = wnnr
        else:
            raise ValueError("Invalid type of specific pragma warning!")


    def __str__(self):
        return "{0:10d} {1:2d}|".format(self._line, self._level) + \
            ("".join(
            [PragmaWarning.cfg_indent for i in range(self._level)]
            ) + self._type + " " + self._wnnr).ljust(PragmaWarning.cfg_ljust) + \
            self._locus

    specific_types = ("Disable", "Error", "Suppress", "Once", "Default")


class PWStructure:


    @staticmethod
    def Msg(text):
        print("PWStructure: " + text)


    def __init__(self, fname):
        self._fname = fname
        file = open(fname)
        self._occurences = list()


        # _pw_count will keep count of the individual directive types furthermore
        # assemble and compile regex for each of the directive types
        self._pw_count = dict()
        self._pw_count["Unknown"] = 0
        self._regex = dict()
        self._regex["Base"] = re.compile(".*#pragma warning.*")
        wrnr_regex = re.compile("\d+")

        for t in PragmaWarning.types:
            self._pw_count[t] = 0
            self._regex[t] = re.compile(
                ".*#pragma warning.*{0}.*".format(t), re.IGNORECASE
                )

        level = 0
        current_locus = ""

        for ln, line in enumerate(file):

            # If a #line directive is encountered -> use it to deterimne locus
            if line.startswith("#line"):
                current_locus = line[line.find("\""):-1]

            # See if the line contains a #pragma warning directive
            elif self._regex["Base"].match(line):
                # Try to match any of the known types of directives
                got_match = False

                for t in PragmaWarning.types:
                    if self._regex[t].match(line):
                        if any(t is v for v in PragmaWarningSpecific.specific_types):
                            self._occurences.extend([
                                PragmaWarningSpecific(ln + 1, t, level, current_locus, wrnr)
                                for wrnr in wrnr_regex.findall(line)])
                        else:
                            self._occurences.append(
                                PragmaWarning(ln + 1, t, level, current_locus)
                                )
                        if t == "Push":
                            level = level + 1
                        if t == "Pop":
                            level = level - 1
                        self._pw_count[t] = self._pw_count[t] + 1
                        # Found match -> exit for loop
                        got_match = True
                        break
                
                if not got_match:
                    # Couldnt match known types -> register unknown directive
                    self._occurences.append(
                        PragmaWarning(ln + 1, "Unknown", level, current_locus)
                        )
                    self._pw_count["Unknown"] = self._pw_count["Unknown"] + 1
                

        file.close()


    def __str__(self):
        return "Input: {0} \n {1} \n".format(self._fname, 120 * "-") + \
                   '\n'.join(["{0:5d}  {1}".format(self._pw_count[t], t)
                        for t in list(self._pw_count.keys())]) + \
                   "\n{0:5d}  TOTAL".format(sum(list(self._pw_count.values()))) + \
                   "\n{0}\n".format(120 * "-") + \
                   '\n'.join([thispp.__str__() for thispp in self._occurences])